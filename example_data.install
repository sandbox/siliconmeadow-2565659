<?php
/**
 * @file
 * example_data.install
 */

/**
 * Implements hook_install().
 */
function example_data_install() {
  drupal_install_schema('example_data');

  // Adoption of table needed for data module use.
  data_include('DataTable');
  $table_obj = DataTable::instance('example_data');
  $adopted = $table_obj->adopt();

  if ($adopted) {
    DataTable::clearCaches();
  }

}

/**
 * Implements hook_uninstall().
 */
function example_data_uninstall() {
  // The following three lines work with the unadopt method of the data module
  // which is introduced with this patch:
  // https://www.drupal.org/node/2565565#comment-10308967.
  data_include('DataTable');
  $table_obj = DataTable::instance('example_data');
  $adopted = $table_obj->unadopt();

  drupal_uninstall_schema('example_data');
}

/**
 * Implements hook_schema().
 */
function example_data_schema() {
  $schema = array();

  // Retailer Data.
  $schema['example_data'] = array(
    'description' => "Stores retailer information.",
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => "Primary key: a unique ID.",
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => "Name of the retailer.",
      ),
      'url' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Retailer website url.",
      ),
      'account_number' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => "Retailer sap account number.",
      ),
      'service_id' => array(
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
        'description' => "Retailer returns service R24,R48.",
      ),
      'message' => array(
        'type' => 'text',
        'size' => 'medium',
        'not null' => FALSE,
        'description' => "Retailer html message.",
      ),
      'address_line1' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Delivery address line 1.",
      ),
      'address_line2' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Delivery address line 2.",
      ),
      'address_line3' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Delivery address line 3",
      ),
      'address_line4' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Delivery address line 4.",
      ),
      'town' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Delivery town.",
      ),
      'postcode' => array(
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
        'description' => "Delivery postcode.",
      ),
      'country' => array(
        'type' => 'varchar',
        'length' => '10',
        'not null' => FALSE,
        'default' => 'GB',
        'description' => "Delivery country.",
      ),
      'email' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => FALSE,
        'description' => "Retailer reporting email address.",
      ),
      'status' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'not null' => TRUE,
        'description' => '0 Created , 1 Suspended , 2 Active , 3 Deleted',
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}
